package com.company.carparking;

public class ParkingSpace {
    // клас потрібен для обліку даних по паркомісцях
    String booked; // заброньовані паркомісця
    int empty; // порожні паркомісця
    int busy; // зайняті паркомісця
    String type; // тип паркомісця (для вантажних машин, для легкових)
    int number; // номер паркомісця
}
