package com.company.internetshop;

public class Goods {
    // каталог товарів
    String type; // тип товару
    String  sex; // розподіл по статі та дорослі - діти тощо
    String size; // розмір товару
    String season; // сезон товару (одягу)
    String year; // рік колекції (одяг)
    double price; // вартість
    boolean availability; // наявність

}
